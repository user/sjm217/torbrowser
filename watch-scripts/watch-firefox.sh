#!/bin/bash

NEXTESR="17\.0\.[567]esr/"
NEXTRAPID="17\.0/|16\.0\.[34]/"

URL="https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/"
urldump=`wget -q $URL -O -`

echo $urldump | egrep $NEXTESR > /dev/null
if [ $? -eq 0 -a ! -f ~/emailt-esr ]
then
  echo "New Firefox ESR release is out at $URL" | mail mikeperry@torproject.org -s "New Firefox 17.x ESR is out!"
  echo "New Firefox ESR release is out at $URL" | mail erinn@torproject.org -s "New Firefox 17.x ESR is out!"
  echo "New Firefox ESR release is out at $URL" | mail sebastian@torproject.org -s "New Firefox 17.x ESR is out!"
  touch ~/emailt-esr
fi

exit

echo $urldump | egrep $NEXTRAPID > /dev/null
if [ $? -eq 0 -a ! -f ~/emailt-rr ]
then
  echo "New Firefox Rapid Release is out at $URL" | mail mikeperry@torproject.org -s "New Firefox Rapid Release is out!"
  echo "New Firefox Rapid Release is out at $URL" | mail erinn@torproject.org -s "New Firefox Rapid Release is out"
  echo "New Firefox Rapid Release is out at $URL" | mail sebastian@torproject.org -s "New Firefox Rapid Release is out!"
  touch ~/emailt-rr
fi


